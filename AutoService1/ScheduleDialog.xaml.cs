﻿using AutoService1.Constant;
using AutoService1.Utils;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Windows.Controls;

namespace AutoService1
{
    /// <summary>
    /// Interaction logic for ScheduleDialog.xaml
    /// </summary>
    public partial class ScheduleDialog : Window
    {
        Schedule currSchedule;
        DateTime? selectedScheduleDataTime;

        public ScheduleDialog(DateTime? dateTime, Schedule selectedSchedule)
        {
            InitializeComponent();
            InitialComboBoxEmployee();
            InitialComboBoxClient();
            InitialComboBoxCar();
            InitialComboBoxStatus();
            InitialComboBoxPurpose();
            currSchedule = selectedSchedule;
            if (dateTime.HasValue)
            {
                //labelDateTime.Content = dateTime.Value.ToString("dddd , MMM dd yyyy , HH:mm");
                if (selectedSchedule != null)
                    dateTimePicker1.Value = selectedSchedule.DateTime;

                else
                    dateTimePicker1.Value = null;


            }
            selectedScheduleDataTime = dateTime;
            currSchedule = selectedSchedule;
            if (currSchedule != null)
            {
                comboBoxClient.SelectedValue = selectedSchedule.ClientId;
                comboBoxEmployee.SelectedValue = selectedSchedule.EmployeeId;
                comboBoxCar.SelectedValue = selectedSchedule.CarId;
                comboBoxPurpose.SelectedValue = selectedSchedule.Purpose;
                comboBoxStatus.SelectedValue = selectedSchedule.Status;
                btCreate.Content = "Update an Appointment";
            }
            else
            {
                btCreate.Content = "Add an Appointment";
            }
        }
        private void InitialComboBoxEmployee()
        {
            List<ComboBoxIntStringPairs> cbp = new List<ComboBoxIntStringPairs>();
            try
            {
                List<Employee> allEmployees = Globals.ctx.Employees.ToList<Employee>();
                foreach (Employee employee in allEmployees)
                {
                    cbp.Add(new ComboBoxIntStringPairs(employee.EmployeeId, employee.FirstName + " " + employee.LastName));
                }
                comboBoxEmployee.DisplayMemberPath = "_Value";
                comboBoxEmployee.SelectedValuePath = "_Key";
                comboBoxEmployee.ItemsSource = cbp;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }
        private void InitialComboBoxClient()
        {
            List<ComboBoxIntStringPairs> cbp = new List<ComboBoxIntStringPairs>();
            try
            {
                List<Client> allClients = Globals.ctx.Clients.ToList<Client>();
                foreach (Client client in allClients)
                {
                    cbp.Add(new ComboBoxIntStringPairs(client.Id, client.FirstName + " " + client.LastName));
                }
                comboBoxClient.DisplayMemberPath = "_Value";
                comboBoxClient.SelectedValuePath = "_Key";
                comboBoxClient.ItemsSource = cbp;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }
        private void InitialComboBoxCar()
        {
            List<ComboBoxIntStringPairs> cbp = new List<ComboBoxIntStringPairs>();
            try
            {
                List<Car> allCars = Globals.ctx.Cars.ToList<Car>();
                foreach (Car car in allCars)
                {
                    cbp.Add(new ComboBoxIntStringPairs(car.CarId, car.VIN));
                }
                comboBoxCar.DisplayMemberPath = "_Value";
                comboBoxCar.SelectedValuePath = "_Key";
                comboBoxCar.ItemsSource = cbp;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }

        private void InitialComboBoxStatus()
        {
            List<ComboBoxStringPairs> cbp = new List<ComboBoxStringPairs>();
            SortedDictionary<string, string> allStatus = Status.GetAllStatus();
            foreach (KeyValuePair<string, string> status in allStatus)
            {
                cbp.Add(new ComboBoxStringPairs(status.Key, status.Value));
            }
            comboBoxStatus.DisplayMemberPath = "_Value";
            comboBoxStatus.SelectedValuePath = "_Key";
            comboBoxStatus.ItemsSource = cbp;
        }
        
        private void InitialComboBoxPurpose()
        {
            List<ComboBoxStringPairs> cbp = new List<ComboBoxStringPairs>();
            SortedDictionary<string, string> allPurpose = Purpose.GetAllStatus();
            foreach (KeyValuePair<string, string> purpose in allPurpose)
            {
                cbp.Add(new ComboBoxStringPairs(purpose.Key, purpose.Value));
            }
            comboBoxPurpose.DisplayMemberPath = "_Value";
            comboBoxPurpose.SelectedValuePath = "_Key";
            comboBoxPurpose.ItemsSource = cbp;
        }


        private void comboBoxClient_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            int selectedClientId = (int)cmb.SelectedValue;

            List<ComboBoxIntStringPairs> cbp = new List<ComboBoxIntStringPairs>();
            try
            {
                var allCars = from car in Globals.ctx.Cars
                                    where car.ClientId.Equals(selectedClientId)
                                    select car;
                foreach (Car car in allCars)
                {
                    cbp.Add(new ComboBoxIntStringPairs(car.CarId, car.VIN));
                }
                comboBoxCar.DisplayMemberPath = "_Value";
                comboBoxCar.SelectedValuePath = "_Key";
                comboBoxCar.ItemsSource = cbp;


        }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }

        private void btCreate_Click(object sender, RoutedEventArgs e)
        {
            if (!selectedScheduleDataTime.HasValue)
            {
                MessageBox.Show("Please select a date and time!");
                return;
            }

            if (comboBoxEmployee.SelectedIndex == -1)
            {
                MessageBox.Show("Please select an employee!");
                return;
            }
            ComboBoxIntStringPairs selectedEmployee = (ComboBoxIntStringPairs)comboBoxEmployee.SelectedItem;
            int selectedEmployeeId = selectedEmployee._Key;

            if (comboBoxClient.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a client!");
                return;
            }
            ComboBoxIntStringPairs selectedClient = (ComboBoxIntStringPairs)comboBoxClient.SelectedItem;
            int selectedClientId = selectedClient._Key;


            if (comboBoxCar.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a car!");
                return;
            }
            ComboBoxIntStringPairs selectedCar = (ComboBoxIntStringPairs)comboBoxCar.SelectedItem;
            int selectedCarId = selectedCar._Key;

            if (comboBoxPurpose.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a visit purpose!");
                return;
            }
            ComboBoxStringPairs selectedPurpose = (ComboBoxStringPairs)comboBoxPurpose.SelectedItem;
            String selectedPurposeNumber = selectedPurpose._Key;

            if (comboBoxStatus.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a status!");
                return;
            }
            ComboBoxStringPairs selectedStatus = (ComboBoxStringPairs)comboBoxStatus.SelectedItem;
            String selectedStatusNumber = selectedStatus._Key;

            //      int currSchedule = selectedSchedule.ScheduleId;
            //      var client = Globals.ctx.Schedules.Single(s => s.ScheduleId == currSchedule);

            if (currSchedule != null)
            {
                Schedule schedule = currSchedule;
                schedule.Purpose = selectedPurposeNumber;
                schedule.Status = selectedStatusNumber;
                schedule.EmployeeId = selectedEmployeeId;
                schedule.CarId = selectedCarId;
                schedule.ClientId = selectedClientId;

                DateTime? selectedDate = dateTimePicker1.Value;
                if (!selectedDate.HasValue) return;

                schedule.DateTime = (DateTime)selectedDate;
                schedule.Task = "";



            }
            else
            {


                Schedule schedule = new Schedule
                {
                    ClientId = selectedClientId,
                    EmployeeId = selectedEmployeeId,
                    CarId = selectedCarId,
                    Purpose = selectedPurposeNumber,
                    Status = selectedStatusNumber,
                    DateTime = (DateTime)selectedScheduleDataTime,
                    //Task not null
                    Task = "",
                };
                Globals.ctx.Schedules.Add(schedule);

            }

            try
            {
                Globals.ctx.SaveChanges();
            }
            catch (FormatException exception)
            {
                MessageBox.Show(exception.Message);
            }

            this.DialogResult = true;
        }
    }
}
