﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using AutoService1.Utils;
using Microsoft.Win32;

namespace AutoService1
{
    /// <summary>
    /// Interaction logic for CarDialog.xaml
    /// </summary>
    public partial class CarDialog : Window
    {
        //Client currClient;
        Car currCar;
        public CarDialog(Car selectedCar)
        {
            InitializeComponent();        // Initialize table
            InitialComboBoxOwnerName();
            InitialComboBoxCarYear();
            currCar = selectedCar;
            if (selectedCar != null)   // Display the information of selected car in corresponding areas
            {
                comboBoxOwnerName.SelectedValue = selectedCar.ClientId;
                tbMake.Text = selectedCar.Make;
                tbModel.Text = selectedCar.Model;
                tbOdometer.Text = selectedCar.Odometer.ToString();
                tbVin.Text = selectedCar.VIN;
                comboBoxYear.SelectedValue = selectedCar.Year;
                imgPicture.Source = Utils.Helpers.byteArrayToBitmapImage(currCar.Photo);

                btAddUpdate.Content = "Update";  // To update the information of selected car
            } 
            else
            {
                btAddUpdate.Content = "Add";     // To add the information of selected car 
            }
        }

        // Initialize owner names in combobox
        private void InitialComboBoxOwnerName()
        {
            List<ComboBoxIntStringPairs> cbp = new List<ComboBoxIntStringPairs>();
            try
            {
                List<Client> allClients = Globals.ctx.Clients.ToList<Client>();
                foreach (Client client in allClients)
                {
                    cbp.Add(new ComboBoxIntStringPairs(client.Id, client.FirstName + " " + client.LastName));
                }
                comboBoxOwnerName.DisplayMemberPath = "_Value";
                comboBoxOwnerName.SelectedValuePath = "_Key";
                comboBoxOwnerName.ItemsSource = cbp;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }

        // Initialize made year of car in combobox
        private void InitialComboBoxCarYear()
        {
            for (int Year = 1950; Year <= DateTime.Now.Year; Year++)
            {
                comboBoxYear.Items.Add(Year.ToString());
            }
        }

        private void btPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imgPicture.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (UriFormatException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }


        // Function of Add/Update button
        private void btAddUpdate_Click(object sender, RoutedEventArgs e)
        {
            string cbOwnerName = comboBoxOwnerName.Text;
            if (String.IsNullOrEmpty(cbOwnerName))
            {
                MessageBox.Show(this, "The client name must not be empty", "Error adding car", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string vinNum = tbVin.Text;
            if (String.IsNullOrEmpty(vinNum))
            {
                MessageBox.Show(this, "The VIN Number must not be empty", "Error adding car", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string make = tbMake.Text;
            if (String.IsNullOrEmpty(make))
            {
                MessageBox.Show(this, "The Make Name must not be empty", "Error adding car", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string model = tbModel.Text;
            if (String.IsNullOrEmpty(model))
            {
                MessageBox.Show(this, "The Model Name must not be empty", "Error adding car", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            int odometer = Int32.Parse(tbOdometer.Text);
            if (String.IsNullOrEmpty(tbOdometer.Text))
            {
                MessageBox.Show(this, "The Odometer must not be empty", "Error adding car", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string year = comboBoxYear.SelectedValue.ToString();
            if (String.IsNullOrEmpty(year))
            {
                MessageBox.Show(this, "The year of car must not be empty", "Error adding car", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            byte[] data = Utils.Helpers.bitmapImageToByteArray((BitmapImage)imgPicture.Source);

            ComboBoxIntStringPairs selectedClient = (ComboBoxIntStringPairs)comboBoxOwnerName.SelectedItem;
            int selectClientId = selectedClient._Key;

            if (currCar != null)
            {
                Car car = currCar;
                car.Make = make;
                car.Model = model;
                car.VIN = vinNum;
                car.Odometer = odometer;
                car.Year = year;
                currCar.Photo = Utils.Helpers.bitmapImageToByteArray((BitmapImage)imgPicture.Source);
                car.ClientId = selectClientId;

            }
            else
            {
                Car car = new Car
                {
                    ClientId = selectClientId,
                    VIN = vinNum,
                    Model = model,
                    Odometer = odometer,
                    Year = year,
                    Make = make,
                };
                Globals.ctx.Cars.Add(car);

            }

            try
            {
                Globals.ctx.SaveChanges();
            }
            catch (FormatException exception)
            {
                MessageBox.Show(exception.Message);
            }

            this.DialogResult = true;
        }
    }
}
