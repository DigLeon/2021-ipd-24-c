﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoService1
{
    public partial class Schedule
    {
        [NotMapped]
        public string DisplayTime
        {
            get
            {
                return DateTime.ToString("dddd , MMM dd yyyy , HH:mm");
            }
        }

        [NotMapped]
        public string DisplayPurpose
        {
            get
            {
                return Constant.Purpose.GetPurposeNameById(Purpose);
            }
        }

        [NotMapped]
        public string DisplayStatus
        {
            get
            {
                return Constant.Status.GetStatusNameById(Status);
            }
        }
    }
    public partial class Client
    {
        [NotMapped]
        public string ClientName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }
        [NotMapped]
        public string ClientAddress
        {
            get
            {
                return $" {City} {State}";
            }
        }
 
        [NotMapped]
        public string CarsNumber
        {
            get
            {
                var carNo = from c in Globals.ctx.Clients
                            join car in Globals.ctx.Cars on c.Id equals car.ClientId
                            select new
                            {
                                
                            };


                //var owner = (from cl in Globals.ctx.Cars.Include("CarsNo") select cl).ToList<Car>();

                // public int?CarsNumber { get { return CarsNo?.Count; } } // compute only, don't store in DB
                //  public ICollection<Car> CarsNo { get; set; } // one to many
                /*

                var CarsNumber = from car in Globals.ctx.Cars
                              where car.ClientId.Equals(ClientName)
                              select car;
                foreach (Car car in CarsNumber)
                {
                    
                }
                */


                return $" {CarsNumber}"; 
            }
        }
    }
    public partial class Employee
    {
        [NotMapped]
        public string EmployeeName
        {
            get
            {
                return $"{FirstName} {LastName}";
            }
        }

        public string EmployeeAddress
        {
            get
            {
                return $" {City} {State}";
            }
        }
        public string EmployeePhone
        {
            get
            {
                return $" {Phone}";
            }
        }
    }
}
