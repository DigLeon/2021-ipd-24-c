﻿using AutoService1.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace AutoService1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GridViewColumnHeader listViewSortCol = null;
        private SortAdorner listViewSortAdorner = null;

        /*---------------------------------------Main Part Start---------------------------------------*/
        public MainWindow()
        {
            InitializeComponent();
            Globals.ctx = new AutoService();
        }

        // Tab click 
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TabSchedule.IsSelected) updateScheduleListView();
            if (TabClients.IsSelected) updateClientListView();
            if (TabCars.IsSelected) updateCarListView();
            if (TabStaff.IsSelected) updateStaffListView();
        }

        //Sort
        public class SortAdorner : Adorner
        {

            public ListSortDirection Direction { get; private set; }

            public SortAdorner(UIElement element, ListSortDirection dir)
                : base(element)
            {
                this.Direction = dir;
            }

        }
        /*---------------------------------------Main Part End---------------------------------------*/


        /*---------------------------------------Schedule Part Start---------------------------------------*/
        // Refresh schedule list
        private void updateScheduleListView()
        {
            
            //Utils.Helpers.AutoResizeColumns(lvSchedule);
            Schedule selectedSchedule = (Schedule)lvSchedule.SelectedItem;
            if (selectedSchedule != null)
            {
                dateTimePicker1.Value = selectedSchedule.DateTime;
            }

            else
            {
                dateTimePicker1.Value = null;
             //   lvSchedule.ItemsSource = (from s in Globals.ctx.Schedules where s.DisplayTime.Equals(dateTimePicker1.Value) select s).ToList<Schedule>();
            }
            lvSchedule.ItemsSource = (from s in Globals.ctx.Schedules select s).ToList<Schedule>();
            lvSchedule.Items.Refresh();
        }


        private void lvSchedule_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /*
            Schedule selectedSchedule = (Schedule)lvSchedule.SelectedItem;
            if (selectedSchedule != null)
                dateTimePicker1.Value = selectedSchedule.DateTime;
            else
                dateTimePicker1.Value = null;
            */
        }

        private void lvSchedule_MouseDoubleClick(object sender, MouseButtonEventArgs e)

        {
            Schedule selectedSchedule = (Schedule)lvSchedule.SelectedItem;

            if (selectedSchedule != null)
                dateTimePicker1.Value = selectedSchedule.DateTime;
            else
                dateTimePicker1.Value = null;

            DateTime? selectedDate = dateTimePicker1.Value;
            if (!selectedDate.HasValue) return;
            //Schedule selectedSchedule = (Schedule)lvSchedule.SelectedItem;

            if (selectedSchedule == null) return;
            ScheduleDialog scheduleDialog = new ScheduleDialog(selectedDate, selectedSchedule);
            if (scheduleDialog.ShowDialog() == true)
            {
                updateScheduleListView();
            }
        }

        // Delete schedule
        private void btDeleteSchedule_Click(object sender, RoutedEventArgs e)
        {
            Schedule selectedSchedule = (Schedule)lvSchedule.SelectedItem;
            if (selectedSchedule == null) return;
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the record?\n", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // action cancelled
            try
            {
                Globals.ctx.Schedules.Remove(selectedSchedule);
                Globals.ctx.SaveChanges();
                updateScheduleListView();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        // Make a new schedule
        private void btAddSchedule_Click(object sender, RoutedEventArgs e)
        {
            DateTime? selectedDate = dateTimePicker1.Value;
            if (selectedDate.HasValue)
            {
                ScheduleDialog addScheduleDialog = new ScheduleDialog(selectedDate, null);
                if (addScheduleDialog.ShowDialog() == true)
                {
                    updateScheduleListView();
                }
            }

        }

        // Sort
        private void lvScheduleColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                lvSchedule.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            lvSchedule.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
        }
        /*---------------------------------------Schedule Part End---------------------------------------*/


        /*---------------------------------------Client Part Start---------------------------------------*/
        // Fetch the data from Clients table
        public void updateClientListView()
        {

            lvClient.ItemsSource = (from c in Globals.ctx.Clients select c).ToList<Client>();
            lvClient.Items.Refresh();

        }
        private void btClientAddUpdate_Click(object sender, RoutedEventArgs e)
        {

            {

                    ClientDialogNew clientDialogNew = new ClientDialogNew(null);
                if (clientDialogNew.ShowDialog() == true)
                {
                    updateClientListView();
                }
            }
        }

        private void lvClient_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Client selectedClient = (Client)lvClient.SelectedItem;
            if (selectedClient == null) return;
            ClientDialogNew clientDialogNew = new ClientDialogNew(selectedClient);
            if (clientDialogNew.ShowDialog() == true)
            {

                updateClientListView();
            }
        }



        private void lvClient_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btClientDelete_Click(object sender, RoutedEventArgs e)
        {
            var selectedClient = (Client)lvClient.SelectedItem;
            int selectedId = selectedClient.Id;
            // var context = new AutoService();
            if (selectedClient != null)
            {
                MessageBoxResult result = MessageBox.Show(this, "Do you want to delete selected car?", "Program message", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        try
                        {
                            Client clientToDelete = (from c in Globals.ctx.Clients where c.Id == selectedId select c).FirstOrDefault<Client>();
                            if (clientToDelete != null)
                            {


                                Globals.ctx.Clients.Remove(clientToDelete); // schedule for deletion from database
                                Globals.ctx.SaveChanges();
                                MessageBox.Show("Record deleted.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);

                                updateClientListView();
                            }
                            else
                            {
                                MessageBox.Show("Record to delete not found.", "Database message", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        catch (SystemException ex)
                        {
                            MessageBox.Show(this, ex.Message, "Database error:", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        Console.WriteLine("This shouldn't happen!");
                        return;
                }

            }
        }
        private void lvUsersColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                lvClient.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            lvClient.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
        }
        /*---------------------------------------Client Part End---------------------------------------*/


        /*---------------------------------------Car Part Start---------------------------------------*/
        // Fetch the data from Cars table
        private void updateCarListView()
        {
            lvCar.ItemsSource = (from car in Globals.ctx.Cars select car).ToList<Car>();
            byte[] data = Utils.Helpers.bitmapImageToByteArray((BitmapImage)CarPhoto.Source);
            lvCar.Items.Refresh();
            //Utils.Helpers.AutoResizeColumns(lvCar);       
        }

        private void btAddCar_Click(object sender, RoutedEventArgs e)
        {
            //Car selectedCar = (Car)lvCar.SelectedItem;
            CarDialog addCarDialog = new CarDialog(null);
            if (addCarDialog.ShowDialog() == true)
            {
                updateCarListView();
            }
        }

        private void lvCar_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Car selectedCar = (Car)lvCar.SelectedItem;

            if (selectedCar == null) return;
            CarDialog carDialog = new CarDialog(selectedCar);
            if (carDialog.ShowDialog() == true)
            {
                updateCarListView();

            }
        }

        private void btDeleteCar_Click(object sender, RoutedEventArgs e)
        {
            Car selectedCar = (Car)lvCar.SelectedItem;
            if (selectedCar == null) return;
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the record?\n", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // action cancelled
            try
            {
                Globals.ctx.Cars.Remove(selectedCar);
                Globals.ctx.SaveChanges();
                updateCarListView();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void lvCar_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Car selectedCar = (Car)lvCar.SelectedItem;
            if (selectedCar != null)
            {
                Client owner = selectedCar.Client;
                OwnerFirstName.Content = owner.FirstName;
                OwnerLastName.Content = owner.LastName;
                OwnerPhone.Content = owner.Phone;
                OwnerEmail.Content = owner.Email;
                CarPhoto.Source = Utils.Helpers.byteArrayToBitmapImage(selectedCar.Photo);
            }
            else
            {
                OwnerFirstName.Content = "";
                OwnerLastName.Content = "";
                OwnerPhone.Content = "";
                OwnerEmail.Content = "";
                CarPhoto.Source = null;
            }
        }
        private void lvCarColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                lvCar.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            lvCar.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
        }

        /*---------------------------------------Car Part End---------------------------------------*/


        /*---------------------------------------Employee Part Start---------------------------------------*/
        // Fetch the data from Employees table
        private void updateStaffListView()
        {
            lvStaff.ItemsSource = (from employee in Globals.ctx.Employees select employee).ToList<Employee>();
            byte[] data = Utils.Helpers.bitmapImageToByteArray((BitmapImage)EmployeePhoto.Source);
            lvStaff.Items.Refresh();
            //Utils.Helpers.AutoResizeColumns(lvCar);
        }

        private void btEmployeeAdd_Click(object sender, RoutedEventArgs e)
        {
            EmployeeDialogNew addEmployeeDialogNew = new EmployeeDialogNew(null);
            if (addEmployeeDialogNew.ShowDialog() == true)
            {
                updateStaffListView();
            }
        }

        private void btEmployeeDelete_Click(object sender, RoutedEventArgs e)
        {
            Employee selectedEmployee = (Employee)lvStaff.SelectedItem;
            if (selectedEmployee == null) return;
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the record?\n", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // action cancelled
            try
            {
                Globals.ctx.Employees.Remove(selectedEmployee);
                Globals.ctx.SaveChanges();
                updateStaffListView();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void lvStaff_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Employee selectedEmployee = (Employee)lvStaff.SelectedItem;
            if (selectedEmployee != null)
            {
                EmployeePhoto.Source = Utils.Helpers.byteArrayToBitmapImage(selectedEmployee.Photo);
            }
            else
            {
                EmployeePhoto.Source = null;
            }
        }
        private void lvStaff_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Employee selectedEmployee = (Employee)lvStaff.SelectedItem;

            if (selectedEmployee == null) return;
            EmployeeDialogNew employeeDialog = new EmployeeDialogNew(selectedEmployee);
            if (employeeDialog.ShowDialog() == true)
            {
                updateStaffListView();
            }
        }

        private void lvStaffColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                lvStaff.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            lvStaff.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));

        }
        /*---------------------------------------Employee Part End---------------------------------------*/

    }
}
