﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoService1
{
    /// <summary>
    /// Interaction logic for ClientDialogNew.xaml
    /// </summary>
    public partial class EmployeeDialogNew : Window
    {
        Employee currEmployee;
        public EmployeeDialogNew (Employee e)
        {
            InitializeComponent();
            currEmployee = e;

            if (currEmployee != null)
            {
                tbFirstName.Text = currEmployee.FirstName;
                tbLastName.Text = currEmployee.LastName;
                tbStreet.Text = currEmployee.Street;
                tbCity.Text = currEmployee.City;
                tbState.Text = currEmployee.State;
                tbZip.Text = currEmployee.Zip;
                tbPhone.Text = currEmployee.Phone;
                tbEmail.Text = currEmployee.Email;
                imgPicture.Source = Utils.Helpers.byteArrayToBitmapImage(currEmployee.Photo);

                btAddUpdate.Content = "Update";
            }
            else
            {
                btAddUpdate.Content = "Add";

            }
        }
        public EmployeeDialogNew()
        {
            InitializeComponent();
        }

        private void btPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imgPicture.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (UriFormatException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }


        private static void FindEmployee()


        {
            /*
                //        HibernatingRhinos.Profilrer.Appender.EntityFramework.EntityFrameworkProfiler.Initalize();
             var clients = GetClientsEf();
             foreach (var client in clients)
             {
             Console.WriteLine("Id:{ 0} \tName:{ 1}", client.Id,client.FirstName);
               }
                 Console.ReadLine();
            */

            var context = new AutoService();
            var employee = context.Employees.Single(e => e.EmployeeId == 1);
            employee.FirstName = "Leon";
            context.SaveChanges();

        }


        private static List<Employee> GetClientsEf()
        {
            var context = new AutoService();
            IQueryable<Employee> query = context.Employees;
            List<Employee> employees = query.ToList();
            // var clients = context.Clients.ToList();
            return employees;
        }

        private void btAddUpdate_Click(object sender, RoutedEventArgs e)
        {
            string nameFirst = tbFirstName.Text;
            if (String.IsNullOrEmpty(nameFirst))
            {
                MessageBox.Show(this, "The first name  must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string nameLast = tbLastName.Text;
            if (String.IsNullOrEmpty(nameLast))
            {
                MessageBox.Show(this, "The last name must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string street = tbStreet.Text;
            if (String.IsNullOrEmpty(street))
            {
                MessageBox.Show(this, "The address must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string city = tbCity.Text;
            if (String.IsNullOrEmpty(city))
            {
                MessageBox.Show(this, "The city must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string state = tbState.Text;
            if (String.IsNullOrEmpty(state))
            {
                MessageBox.Show(this, "The state must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string zip = tbZip.Text;
            if (String.IsNullOrEmpty(zip))
            {
                MessageBox.Show(this, "The zip must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string phone = tbPhone.Text;
            if (String.IsNullOrEmpty(phone))
            {
                MessageBox.Show(this, "The phone must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string email = tbEmail.Text;
            if (String.IsNullOrEmpty(email))
            {
                MessageBox.Show(this, "The email must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            byte[] photoData = Utils.Helpers.bitmapImageToByteArray((BitmapImage)imgPicture.Source);

            var FullName = nameFirst + " " + nameLast;


            if (currEmployee != null)
            {
                Employee employee = currEmployee;
                employee.FirstName = nameFirst;
                employee.LastName = nameLast;
                employee.Street = street;
                employee.City = city;
                employee.State = state;
                employee.Zip = zip;
                employee.Phone = phone;
                employee.Email = email;
                currEmployee.Photo = photoData;
                employee.Photo = photoData;
            }
            else
            {
                Employee employee = new Employee
                {
                    FirstName = nameFirst,
                    LastName = nameLast,
                    Street = street,
                    City = city,
                    State = state,
                    Zip = zip,
                    Phone = phone,
                    Email = email,
                    Photo = photoData,
                };

                Globals.ctx.Employees.Add(employee);
            }

            try
            {
                Globals.ctx.SaveChanges();
            }
            catch (FormatException exception)
            {
                MessageBox.Show(exception.Message);
            }

            this.DialogResult = true;
        }

        private void btClientAdd_Click(object sender, RoutedEventArgs e)
        {

        }
    }

}
