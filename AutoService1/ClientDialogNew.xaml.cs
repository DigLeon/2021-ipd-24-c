﻿using AutoService1.Properties;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoService1
{
    /// <summary>
    /// Interaction logic for ClientDialogNew.xaml
    /// </summary>
    public partial class ClientDialogNew : Window

    {

         Client currClient;
        public ClientDialogNew(Client c)
        {

            InitializeComponent();
            currClient = c;

           if (c != null)
            {
                tbFirstName.Text = currClient.FirstName;
                tbLastName.Text = currClient.LastName;
                tbStreet.Text = currClient.Street;
                tbCity.Text = currClient.City;
                tbState.Text = currClient.State;
                tbZip.Text = currClient.Zip;
                tbPhone.Text = currClient.Phone;
                tbEmail.Text = currClient.Email;
                imgPicture.Source = byteArrayToBitmapImage(currClient.Photo);
                btClientAdd.Content = "Update";


         }
          else
          {
               btClientAdd.Content = "Add";

           }

        }
        public ClientDialogNew()
        {
            InitializeComponent();




        }
        private BitmapImage byteArrayToBitmapImage(byte[] byteImage)
        {
            if (byteImage == null) return null;
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.StreamSource = new System.IO.MemoryStream(byteImage);
            image.EndInit();
            return image;
        }
        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }
        private void btPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imgPicture.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (UriFormatException ex)
                {
                    MessageBox.Show(this, ex.Message, "Error opening file:", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private static List<Client> GetClientsEf()
        {
            var context = new AutoService();
            IQueryable<Client> query = context.Clients;
            List<Client> clients = query.ToList();
            // var clients = context.Clients.ToList();
            return clients;
        }

        /*
        private static void CarNum()
        {
           
            var allCars = from car in Globals.ctx.Cars
                          where car.ClientId.Equals()
                          select car;
            foreach (Car car in allCars)
            {
               // cbp.Add(new ComboBoxIntStringPairs(car.CarId, car.VIN));
            }

        }
        */
        private void btAddUpdate_Click(object sender, RoutedEventArgs e)
        {
            {

                try
                {
                    string nameFirst = tbFirstName.Text;
                    if (String.IsNullOrEmpty(nameFirst))
                    {
                        MessageBox.Show(this, "The first name  must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    string nameLast = tbLastName.Text;
                    if (String.IsNullOrEmpty(nameLast))
                    {
                        MessageBox.Show(this, "The last name must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    string street = tbStreet.Text;
                    if (String.IsNullOrEmpty(street))
                    {
                        MessageBox.Show(this, "The address must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    string city = tbCity.Text;
                    if (String.IsNullOrEmpty(city))
                    {
                        MessageBox.Show(this, "The city must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    string state = tbState.Text;
                    if (String.IsNullOrEmpty(state))
                    {
                        MessageBox.Show(this, "The state must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    string zip = tbZip.Text;
                    if (String.IsNullOrEmpty(zip))
                    {
                        MessageBox.Show(this, "The zip must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    string phone = tbPhone.Text;
                    if (String.IsNullOrEmpty(phone))
                    {
                        MessageBox.Show(this, "The phone must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    string email = tbEmail.Text;
                    if (String.IsNullOrEmpty(email))
                    {
                        MessageBox.Show(this, "The email must not be empty", "Error creating person", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    byte[] data = bitmapImageToByteArray((BitmapImage)imgPicture.Source);

                    if (currClient != null)
                    {
                        int selectedId = currClient.Id;

                        var allCars = from car in Globals.ctx.Cars
                                      where car.ClientId.Equals(currClient.Id)
                                      select car;

                        var client = Globals.ctx.Clients.Single(c => c.Id == selectedId);
                        client.FirstName = tbFirstName.Text;
                        client.FirstName = tbFirstName.Text;
                        client.LastName = tbLastName.Text;
                        client.Street = tbStreet.Text;
                        client.City = tbCity.Text;
                        client.State = tbState.Text;
                        client.Zip = tbZip.Text;
                        client.Phone = tbPhone.Text;
                        client.Email = tbEmail.Text;
                        client.Photo = bitmapImageToByteArray((BitmapImage)imgPicture.Source);
                       

                       

                    }
                    else

                    {
                        Globals.ctx.Clients.Add(new Client { FirstName = nameFirst, LastName = nameLast, Street = street, City = city, State = state, Zip = zip, Phone = phone, Email = email });
                       
                       
                    }

                    Globals.ctx.SaveChanges(); // ex SystemException

            }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "error saving record", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                this.DialogResult = true;

            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
