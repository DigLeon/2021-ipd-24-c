﻿using System.Collections.Generic;

namespace AutoService1.Constant
{
    public static class Status
    {
        public const string 
            OPEN = "101",
            APPROVED = "103",
            UNAPPROVED = "105",
            PROCESSING = "107",
            RESOLVED = "109",
            BLOCKED = "111",
            QA = "113",
            CLOSED = "115";

        public static SortedDictionary<string, string> GetAllStatus()
        {
            return new SortedDictionary<string, string>

            {
                { OPEN, "Open" },
                { APPROVED, "Approved" },
                { UNAPPROVED, "Unapproved" },
                { PROCESSING, "Processing" },
                { RESOLVED, "Resolved" },
                { BLOCKED, "Blocked" },
                { QA, "QA" },
                { CLOSED, "Closed" }
            };
        }

        public static string GetStatusNameById(string key)
        {
            if (GetAllStatus().ContainsKey(key))
            {
                return GetAllStatus()[key];
            }
            return "";
        }
    }
}
