﻿using System.Collections.Generic;

namespace AutoService1.Constant
{
    public static class Purpose
    {
        public const string
            BRAKES = "101",
            TIRE_AND_ALIGNMENT = "103",
            MUFFLERS_AND_EXHAUST = "105",
            ENGINE_AND_FILTER = "107",
            HEATING_AND_COOLING = "109",
            STEERING_AND_SUSPENSION = "111",
            TRANSMISSION = "113",
            BATTERY_AND_ELECTRICAL_SYSTEM = "115",
            LUBRICANT_OIL_AND_FILTER = "117";

        public static SortedDictionary<string, string> GetAllStatus()
        {
            return new SortedDictionary<string, string>

            {
                { BRAKES, "Brakes" },
                { TIRE_AND_ALIGNMENT, "Tire and alignment" },
                { MUFFLERS_AND_EXHAUST, "Mufflers and exhaust" },
                { ENGINE_AND_FILTER, "Engine and Filter" },
                { HEATING_AND_COOLING, "Heating and Cooling" },
                { STEERING_AND_SUSPENSION, "Steering and Suspension" },
                { TRANSMISSION, "Transmission" },
                { BATTERY_AND_ELECTRICAL_SYSTEM, "Battery and Electrical System" },
                { LUBRICANT_OIL_AND_FILTER, "Lubricant Oil and Filter" }
            };
        }

        public static string GetPurposeNameById(string key)
        {
            if (GetAllStatus().ContainsKey(key))
            {
                return GetAllStatus()[key];
            }
            return "";
        }
    }
}
