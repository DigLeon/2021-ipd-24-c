﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoService1.Utils
{
    // For combobox string-string pairs
    public class ComboBoxStringPairs
    {
        public string _Key { get; set; }
        public string _Value { get; set; }

        public ComboBoxStringPairs(string _key, string _value)
        {
            _Key = _key;
            _Value = _value;
        }
    }

    // For combobox int-string pairs
    public class ComboBoxIntStringPairs
    {
        public int _Key { get; set; }
        public string _Value { get; set; }

        public ComboBoxIntStringPairs(int _key, string _value)
        {
            _Key = _key;
            _Value = _value;
        }
    }
}
